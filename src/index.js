/**
 *
 * @module fun-unfold
 */
;(() => {
  'use strict'

  /* imports */
  const { inputs } = require('guarded')
  const curry = require('fun-curry')

  /**
   *
   * @function module:fun-unfold.unfold
   *
   * @param {Function} next - generates the next value from the previous value
   * @param {Function} stop - predicate function for stopping condition
   * @param {*} value - initial value
   *
   * @return {*} the final unfolded value
   */
  const unfold = (next, stop, value) => {
    while (!stop(value)) {
      value = next(value)
    }

    return value
  }

  const tuple = (fs, a) => fs.length === a.length &&
    fs.reduce((r, f, i) => r && f(a[i]), true)
  const fun = f => typeof f === 'function'
  const t = () => true

  /* exports */
  module.exports = curry(inputs(curry(tuple)([fun, fun, t]), unfold))
})()

