# [fun-unfold](https://bagrounds.gitlab.io/fun-unfold)

Unfold a complex value from a generator, predicate, and seed

[![build-status](https://gitlab.com/bagrounds/fun-unfold/badges/master/build.svg)](https://gitlab.com/bagrounds/fun-unfold/commits/master)
[![coverage-report](https://gitlab.com/bagrounds/fun-unfold/badges/master/coverage.svg)](https://gitlab.com/bagrounds/fun-unfold/commits/master)
[![license](https://img.shields.io/npm/l/fun-unfold.svg)](https://www.npmjs.com/package/fun-unfold)
[![version](https://img.shields.io/npm/v/fun-unfold.svg)](https://www.npmjs.com/package/fun-unfold)
[![downloads](https://img.shields.io/npm/dt/fun-unfold.svg)](https://www.npmjs.com/package/fun-unfold)
[![downloads-monthly](https://img.shields.io/npm/dm/fun-unfold.svg)](https://www.npmjs.com/package/fun-unfold)
[![dependencies](https://david-dm.org/bagrounds/fun-unfold/status.svg)](https://david-dm.org/bagrounds/fun-unfold)

## [Test Coverage](https://bagrounds.gitlab.io/fun-unfold/coverage/lcov-report/index.html)

## [API Docs](https://bagrounds.gitlab.io/fun-unfold/index.html)

## Dependencies

### Without Tests

![Dependencies](https://bagrounds.gitlab.io/fun-unfold/img/dependencies.svg)

### With Tests

![Test Dependencies](https://bagrounds.gitlab.io/fun-unfold/img/dependencies-test.svg)

