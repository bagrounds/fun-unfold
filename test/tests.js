;(() => {
  'use strict'

  /* imports */
  const { sync } = require('fun-test')
  const curry = require('fun-curry')

  const suc = x => x + 1
  const gt10 = x => x > 10
  const equal = (x, y) => x === y
  const tests = [
    {
      inputs: [suc, gt10, 0],
      predicate: curry(equal)(11)
    }
  ]

  /* exports */
  module.exports = tests.map(sync)
})()

